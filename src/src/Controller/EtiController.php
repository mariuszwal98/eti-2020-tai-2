<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class EtiController
 */
class EtiController extends AbstractController
{
    /**
     * @Route("/first/page")
     */
    public function randomNumber()
    {
        $number = random_int(0, 100);

        return $this->render('eti/first_page.html.twig', [
            'number' => $number,
        ]);
    }
}